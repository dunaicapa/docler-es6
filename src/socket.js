class Socket {

  constructor (options = {}) {
    this.setOptions(options);
    this.socket = this.initSocket();
  }

  setOptions () {
    Object.assign(this, ...arguments);
  }

  initSocket () {
    return io.connect(this.host);
  }

  on (event, callback) {
    this.socket.on(event, callback);
  }

  emit (name, data) {
    this.socket.emit(name, data);
  }
}

// export default Socket
