class Chat {

  constructor (options = {}) {
    this.setOptions(this.getDefaultParams(), options);
    this.bind();
  }

  bind () {
    this.socket.on('message', (data) => this.renderChatMessage(data));
    this.$inputMessage.onkeypress = (event) => (event.key === 'Enter') ? this.sendEvent() : null;
    this.$submitButton.onclick = () => this.sendEvent();
    this.$inputUser.value = this.username;
    this.$inputUser.onkeyup = () => (this.username = this.$inputUser.value);
  }

  setOptions () {
    Object.assign(this, ...arguments);
  }

  getDefaultParams () {
    return {
      host: null,
      revert: false,
      username: 'guest0001',
      $inputUser: document.getElementById('inputUser'),
      $inputMessage: document.getElementById('inputMessage'),
      $submitButton: document.getElementById('submitButton'),
      $messagesContainer: document.getElementById('messages'),
    };
  }

  getMessageContainer (data) {
    const $messageContainer = document.createElement('div');
    $messageContainer.className += ' message ';
    if(data.user === this.username) {
      $messageContainer.className += ' own ';
    } else {
      const $nameSpan = document.createElement('span');
      $nameSpan.innerHTML = `${data.user}: `;
      $nameSpan.className = ` user `;
      $messageContainer.appendChild($nameSpan);
    }
    const $messageSpan = document.createElement('span');
    $messageSpan.innerHTML = data.message;
    $messageContainer.appendChild($messageSpan);
    return $messageContainer;
  }

  renderChatMessage (data) {
    if(this.revert) {
      this.$messagesContainer.insertBefore(this.getMessageContainer(data), this.$messagesContainer.firstChild);
    } else {
      this.$messagesContainer.appendChild(this.getMessageContainer(data));
    }
  }

  sendEvent () {
    this.sendChatMessage(this.$inputMessage.value);
    this.$inputMessage.value = '';
    this.$inputMessage.focus();
  }

  sendChatMessage (message) {
    const data = {
      message: message,
      user: this.username
    };
    this.socket.emit('message', data);
    this.renderChatMessage(data);
  }
}

// export default Chat
